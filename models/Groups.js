'use strict';
module.exports = mongoose => {
  const newSchema = new mongoose.Schema({
    name: {
      type: String
    },
    desc: {
      type: String
    },
    isactive: {
      type: Boolean
    }
  }, {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  });
  const Groups = mongoose.model('Groups', newSchema);
  return Groups;
};